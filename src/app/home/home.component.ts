import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import {Observer} from 'rxjs/Observer';
import {Subscription} from 'rxjs/Subscription';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  nuObSub: Subscription;
  cuObSub: Subscription;
  constructor() { }

  ngOnInit() {
    const myNumber = Observable.interval(1000)
      .map(
        (data: number) => {
          return data * 2;
        }
      );
    this.nuObSub = myNumber.subscribe(
      (number: number) => {
        console.log(number);
      }
    );

    const myObservable = Observable.create(
      (observer: Observer<string>) => {
        setTimeout(() => {
          observer.next('first Package');
        }, 2000);
        setTimeout(() => {
          observer.next('Second Package');
        }, 3000);
        setTimeout(() => {
          observer.complete();
        }, 3000);
        setTimeout(() => {
          observer.next('this does not work');
        }, 5000);
      }
    );
    this.cuObSub = myObservable.subscribe(
      (data: string) => { console.log(data); },
      (error: string) => { console.log(error); },
      () => { console.log('completed'); }
    );
  }

  ngOnDestroy() {
    this.nuObSub.unsubscribe();
    this.cuObSub.unsubscribe();
  }

}
